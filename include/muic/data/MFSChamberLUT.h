#ifndef MUICSIMULATION_MFSCHAMBERLUT_H
#define MUICSIMULATION_MFSCHAMBERLUT_H


#include <map>
#include <string>

namespace muic {

    class MFSChamberLUT {

    public:
        MFSChamberLUT();

        ~MFSChamberLUT() = default;

        std::string getDetId(const int &chamber_id) const;

        int getStation(const std::string &) const;

        int getChamber(const std::string &) const;

    private:
        std::map<int, std::string> id_to_detid_;
        std::map<std::string, int> pv_to_station_;
        std::map<std::string, int> pv_to_chamber_;
    };

}

#endif //MUICSIMULATION_MFSCHAMBERLUT_H
