#ifndef MUICSIMULATION_MUICCONTEXT_H
#define MUICSIMULATION_MUICCONTEXT_H


#include "muic/analysis/NtupleMaker.h"
#include "muic/analysis/GenParticleCollector.h"
#include "muic/data/MFSChamberLUT.h"

namespace muic {
    class MuICContext {
    public:
        MuICContext();

        ~MuICContext();

        // Collectors
        NtupleMaker ntuple_maker_;
        GenParticleCollector gen_particle_collector_;
    };
}


#endif //MUICSIMULATION_MUICCONTEXT_H
