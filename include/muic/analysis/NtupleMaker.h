#ifndef MuIC_NtupleMaker_h
#define MuIC_NtupleMaker_h

#include "globals.hh"
#include "muic/sensitive/MFSHit.h"
#include <vector>


class TFile;

class TTree;

namespace muic {
    class NtupleMaker {
    public:
        NtupleMaker();

        ~NtupleMaker();

        void onRunStart();

        void onRunEnd();

        void onEventStart();

        void addGenMuon(
                const G4double &,
                const G4double &,

                const G4double &,
                const G4double &,
                const G4double &,
                const G4double &,

                const G4double &,
                const G4double &,
                const G4double &,
                const G4double &
        );

        void addHit(const muic::MFSHit &hit);

        void onEventEnd();

    private:
        // Metadata
        int n_events_ = 0;

        // Events
        int n_gen_ = 0;

        std::vector<double> gen_e_after_nozzle_;
        std::vector<double> gen_e_before_nozzle_;
        std::vector<double> gen_e_deposited_nozzle_;

        std::vector<double> gen_pos_eta_before_nozzle_;
        std::vector<double> gen_pos_eta_after_nozzle_;
        std::vector<double> gen_pos_eta_scatter_nozzle_;

        std::vector<double> gen_mom_eta_before_nozzle_;
        std::vector<double> gen_mom_eta_after_nozzle_;
        std::vector<double> gen_mom_eta_scatter_nozzle_;

        std::vector<double> gen_pos_phi_before_nozzle_;
        std::vector<double> gen_pos_phi_after_nozzle_;
        std::vector<double> gen_pos_phi_scatter_nozzle_;

        std::vector<double> gen_mom_phi_before_nozzle_;
        std::vector<double> gen_mom_phi_after_nozzle_;
        std::vector<double> gen_mom_phi_scatter_nozzle_;

        int n_hit_ = 0;

        std::vector<int> hit_station;
        std::vector<int> hit_chamber;
        std::vector<double> hit_energy;
        std::vector<double> hit_glob_x;
        std::vector<double> hit_glob_y;
        std::vector<double> hit_glob_z;
        std::vector<double> hit_glob_t;
        std::vector<double> hit_glob_eta;
        std::vector<double> hit_glob_phi;
    };
}


#endif
