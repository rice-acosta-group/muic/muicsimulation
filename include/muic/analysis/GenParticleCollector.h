//
// Created by om15 on 5/24/23.
//

#ifndef MUIC_ANALYSIS_GENPARTICLECOLLECTOR_H
#define MUIC_ANALYSIS_GENPARTICLECOLLECTOR_H


#include <map>
#include <G4Types.hh>

class GenParticleCollector {

public:
    GenParticleCollector();

    ~GenParticleCollector();

    void onEventStart();

    void onEventEnd();

    void setEnergyBeforeNozzle(const unsigned int &, const G4double &);

    void setEnergyAfterNozzle(const unsigned int &, const G4double &);

    void addEnergyDeposit(const unsigned int &, const G4double &);

    void setEtaBeforeNozzle(const unsigned int &, const G4double &, const G4double &);

    void setEtaAfterNozzle(const unsigned int &, const G4double &, const G4double &);

    void setPhiBeforeNozzle(const unsigned int &, const G4double &, const G4double &);

    void setPhiAfterNozzle(const unsigned int &, const G4double &, const G4double &);

public:
    std::map<unsigned int, double> energy_before_nozzle_collection_;
    std::map<unsigned int, double> energy_after_nozzle_collection_;
    std::map<unsigned int, double> energy_deposit_collection_;

    std::map<unsigned int, double> pos_eta_before_nozzle_collection_;
    std::map<unsigned int, double> pos_eta_after_nozzle_collection_;
    std::map<unsigned int, double> mom_eta_before_nozzle_collection_;
    std::map<unsigned int, double> mom_eta_after_nozzle_collection_;

    std::map<unsigned int, double> pos_phi_before_nozzle_collection_;
    std::map<unsigned int, double> pos_phi_after_nozzle_collection_;
    std::map<unsigned int, double> mom_phi_before_nozzle_collection_;
    std::map<unsigned int, double> mom_phi_after_nozzle_collection_;
};


#endif //MUIC_ANALYSIS_GENPARTICLECOLLECTOR_H
