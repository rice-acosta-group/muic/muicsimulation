#ifndef MUIC_CSC_H
#define MUIC_CSC_H


#include "G4PVPlacement.hh"
#include "G4VPhysicalVolume.hh"
namespace muic {

    std::vector<G4PVPlacement *> create_csc(G4VPhysicalVolume *world);

}


#endif //MUIC_CSC_H