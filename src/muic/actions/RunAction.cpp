#include "muic/MuICContext.h"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"

#include "muic/actions/RunAction.h"

namespace muic {
    RunAction::RunAction(MuICContext *context) :
            G4UserRunAction(),
            context_(context) {
        // Do Nothing
    }

    RunAction::~RunAction() {
        // Do Nothing
    }

    void RunAction::BeginOfRunAction(const G4Run *run) {
        G4cout << "Begin Run " << run->GetRunID() << " start." << G4endl;

        // Start run in NM
        context_->ntuple_maker_.onRunStart();
    }

    void RunAction::EndOfRunAction(const G4Run *run) {
        // End run in NM
        context_->ntuple_maker_.onRunEnd();
    }
}

