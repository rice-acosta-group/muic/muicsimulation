#include "G4Event.hh"
#include "G4ios.hh"
#include <G4SDManager.hh>
#include "muic/MuICContext.h"

#include "muic/actions/EventAction.h"

namespace muic {
    EventAction::EventAction(MuICContext *context)
            : G4UserEventAction(), context_(context) {
        print_every_n_events_ = 100;
    }


    EventAction::~EventAction() {
        // Do Nothing
    }


    void EventAction::BeginOfEventAction(const G4Event *evt) {
        G4int event_id = evt->GetEventID();

        if (event_id % print_every_n_events_ == 0) {
            G4cout << "Begin of event: " << event_id << G4endl;
        }

        // Propagate Event Start Signal
        context_->ntuple_maker_.onEventStart();
        context_->gen_particle_collector_.onEventStart();
    }


    void EventAction::EndOfEventAction(const G4Event *evt) {
        // Save Nozzle Muons
        for (const auto &[track_id, energy_deposited]: context_->gen_particle_collector_.energy_deposit_collection_) {
            const auto &energy_before = context_->gen_particle_collector_.energy_before_nozzle_collection_[track_id];
            const auto &energy_after = context_->gen_particle_collector_.energy_after_nozzle_collection_[track_id];

            const auto &pos_eta_before = context_->gen_particle_collector_.pos_eta_before_nozzle_collection_[track_id];
            const auto &pos_eta_after = context_->gen_particle_collector_.pos_eta_after_nozzle_collection_[track_id];
            const auto &mom_eta_before = context_->gen_particle_collector_.mom_eta_before_nozzle_collection_[track_id];
            const auto &mom_eta_after = context_->gen_particle_collector_.mom_eta_after_nozzle_collection_[track_id];

            const auto &pos_phi_before = context_->gen_particle_collector_.pos_phi_before_nozzle_collection_[track_id];
            const auto &pos_phi_after = context_->gen_particle_collector_.pos_phi_after_nozzle_collection_[track_id];
            const auto &mom_phi_before = context_->gen_particle_collector_.mom_phi_before_nozzle_collection_[track_id];
            const auto &mom_phi_after = context_->gen_particle_collector_.mom_phi_after_nozzle_collection_[track_id];

            context_->ntuple_maker_.addGenMuon(
                    energy_before,
                    energy_after,

                    pos_eta_before,
                    pos_eta_after,
                    mom_eta_before,
                    mom_eta_after,

                    pos_phi_before,
                    pos_phi_after,
                    mom_phi_before,
                    mom_phi_after
            );
        }

        // Get Sensitive Detector
        auto *sd_manager = G4SDManager::GetSDMpointer();

        // Get MFS Hit Collection
        if (mfs_hit_collection_id_ < 0) {
            mfs_hit_collection_id_ = sd_manager->GetCollectionID("mfs/MFSHit");
        }

        // Get Hit Collection
        auto *hit_collection = evt->GetHCofThisEvent();

        if (!hit_collection) {
            return;
        }

        // Get Hits
        auto *mfs_hit_collection = dynamic_cast<MFSHitCollection *>(
                hit_collection->GetHC(mfs_hit_collection_id_)
        );

        if (mfs_hit_collection) {
            for (auto hit: *mfs_hit_collection->GetVector()) {
                context_->ntuple_maker_.addHit(*hit);
            }
        }


        // Propagate Event End Signal
        context_->gen_particle_collector_.onEventEnd();
        context_->ntuple_maker_.onEventEnd();
    }
}