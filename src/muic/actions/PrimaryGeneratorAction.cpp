#include <stdlib.h>
#include <G4Event.hh>
#include <G4Gamma.hh>
#include <G4GeneralParticleSource.hh>
#include <G4ParticleTable.hh>
#include <G4SystemOfUnits.hh>

#include "muic/actions/PrimaryGeneratorAction.h"
#include "muic/MuICConstants.h"

namespace muic {
    PrimaryGeneratorAction::PrimaryGeneratorAction()
            : G4VUserPrimaryGeneratorAction() {
        gps = new G4GeneralParticleSource();

        // Get particle
        auto *particle = G4ParticleTable::GetParticleTable()->FindParticle("mu-");

        // Short-Circuit: Failed to find particle
        if (particle == nullptr) {
            return;
        }

        // G4ParticleDefinition* particle = G4Gamma::Definition();
        gps->SetParticleDefinition(particle);

        // Source will be a point
        gps->GetCurrentSource()->GetPosDist()->SetPosDisType("Point");

        // We will assume all particles are mono energetic
        gps->GetCurrentSource()->GetEneDist()->SetEnergyDisType("Mono");
        gps->GetCurrentSource()->GetEneDist()->SetMonoEnergy(10 * GeV);

        // Define opening angles
        gps->GetCurrentSource()->GetAngDist()->SetAngDistType("iso");
        gps->GetCurrentSource()->GetAngDist()->SetMinTheta(PGUN_THETA_MIN);
        gps->GetCurrentSource()->GetAngDist()->SetMaxTheta(PGUN_THETA_MAX);
    }

    PrimaryGeneratorAction::~PrimaryGeneratorAction() {
        delete gps;
    }

    void PrimaryGeneratorAction::GeneratePrimaries(G4Event *event) {
        // Uncomment the following block to enable flat eta distribution
        double eta = (((double) rand()) / RAND_MAX) * (PGUN_ETA_MAX - PGUN_ETA_MIN) + PGUN_ETA_MIN;
        double theta = 2. * atan(exp(-eta)) * rad;

        gps->GetCurrentSource()->GetAngDist()->SetMaxTheta(theta);
        gps->GetCurrentSource()->GetAngDist()->SetMinTheta(theta);

        gps->GeneratePrimaryVertex(event);
    }

}
