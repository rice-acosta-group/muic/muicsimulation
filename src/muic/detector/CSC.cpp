#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4Trd.hh"
#include "G4Box.hh"
#include "G4IntersectionSolid.hh"

#include "muic/detector/CSC.h"
#include "muic/MuICConstants.h"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

std::vector<G4PVPlacement *> muic::create_csc(G4VPhysicalVolume *world) {
    auto *nist = G4NistManager::Instance();

    // Materials
    auto *detector_material = nist->FindOrBuildMaterial("G4_Al");
    auto *detector_material_2 = nist->FindOrBuildMaterial("G4_Pb");

    // Build big trapezoid
    double big_opening_ang = 22 * deg;
    double big_thickness = 20 * cm;
    double big_outer_radius = 70 * cm;
    double big_inner_radius = 10 * cm;

    // Build small trapezoid
    double small_opening_ang = 21 * deg;
    double small_thickness = 18 * cm;
    double small_outer_radius = 69 * cm;
    double small_inner_radius = 11 * cm;

    auto *big_trape_shape = new G4Trd(
            "big_trape", big_inner_radius * std::tan(big_opening_ang / 2),
            (big_inner_radius + big_outer_radius) * std::tan(big_opening_ang / 2),
            big_thickness / 2, big_thickness / 2,
            big_outer_radius / 2
    );

    auto *small_trape_shape = new G4Trd(
            "small_trape", small_inner_radius * std::tan(small_opening_ang / 2),
            (small_inner_radius + small_outer_radius) * std::tan(small_opening_ang / 2),
            small_thickness / 2, small_thickness / 2,
            small_outer_radius / 2
    );

    auto *shell_shape = new G4SubtractionSolid(
            "shell", big_trape_shape, small_trape_shape);

    //Build anode wire plane
    auto *wire_plane = new G4Trd(
            "wire_plane", small_inner_radius * std::tan(small_opening_ang / 2),
            (small_inner_radius + small_outer_radius) * std::tan(small_opening_ang / 2),
            0.5 * mm / 2, 0.5 * mm / 2,
            small_outer_radius / 2
    );

    //Build strip
    auto *strip = new G4Box("rectangle_strip",
                            (small_inner_radius + small_outer_radius) * std::tan(small_opening_ang / 2),
                            0.5 * mm, 2.5 * mm);

    G4ThreeVector translation(0.0, 0.0, (small_outer_radius / 2 - 0.5 * cm));

    auto *trape_strip = new G4IntersectionSolid("trape_strip",
                                                strip, wire_plane,
                                                G4Translate3D(translation));


    double plane_separation = ((15. / 7) + 0.5) * cm;

    // Build Detectors
    std::vector<G4PVPlacement *> detectors;

    // create logical volume of shell
    auto *shell_lv = new G4LogicalVolume(
            shell_shape, detector_material, "csc_detector"
    );

    // create logical volume of plane
    auto *plane_lv = new G4LogicalVolume(
            wire_plane, detector_material, "anode_plane");

    // create logical volume of strip
    auto *strip_lv = new G4LogicalVolume(
            trape_strip, detector_material_2, "trape_stripe"
    );

    G4VisAttributes *visAttributes = new G4VisAttributes(G4Colour(1.0, 0.0, 0.0));  //
    strip_lv->SetVisAttributes(visAttributes);

    //place strip on plane
    auto *plane_with_strip = new G4PVPlacement(
            nullptr, G4ThreeVector(0, 1 * mm, (small_outer_radius - 0.5 * cm)), strip_lv, "plane+strip", plane_lv,
            false, 0, true);




    // place each of the six planes within shell
//    for (int i = 1; i <= 6; ++i) {
//        double y_coor = 9. * cm - (plane_separation * i);
//
//        auto *shell_with_plane = new G4PVPlacement(
//            nullptr, G4ThreeVector(0.0, y_coor, 0.0), plane_lv, "shell+plane", shell_lv, false, 0, true);
//
//    }

    auto *detector = new G4PVPlacement(
            nullptr, G4ThreeVector(0.0, 0.0, 0.0), strip_lv, "shell_pv", world->GetLogicalVolume(), false, 0, true);

    detectors.push_back(detector);

    return detectors;
}