//
// Created by om15 on 5/24/23.
//

#include <G4ios.hh>
#include <G4SystemOfUnits.hh>
#include "muic/analysis/GenParticleCollector.h"

GenParticleCollector::GenParticleCollector() {
    // Do Nothing
}

GenParticleCollector::~GenParticleCollector() {
    // Do Nothing
}

void GenParticleCollector::onEventStart() {
    // Reset collection
    energy_before_nozzle_collection_.clear();
    energy_after_nozzle_collection_.clear();
    energy_deposit_collection_.clear();
    pos_eta_before_nozzle_collection_.clear();
    mom_eta_before_nozzle_collection_.clear();
    pos_eta_after_nozzle_collection_.clear();
    mom_eta_after_nozzle_collection_.clear();
    pos_phi_before_nozzle_collection_.clear();
    mom_phi_before_nozzle_collection_.clear();
    pos_phi_after_nozzle_collection_.clear();
    mom_phi_after_nozzle_collection_.clear();
}

void GenParticleCollector::onEventEnd() {
    for (auto &[track_id, deposit]: energy_deposit_collection_) {
        G4cout << "Track Id: " << track_id
               << " Deposit: " << deposit / GeV
               << " E Before: " << energy_before_nozzle_collection_[track_id] / GeV
               << " E After: " << energy_after_nozzle_collection_[track_id] / GeV
               << " E Loss: "
               << (energy_before_nozzle_collection_[track_id] - energy_after_nozzle_collection_[track_id]) / GeV
               << " Eta Before: " << pos_eta_before_nozzle_collection_[track_id]
               << " Eta After: " << pos_eta_after_nozzle_collection_[track_id]
               << " Delta Eta: "
               << (pos_eta_after_nozzle_collection_[track_id] - pos_eta_before_nozzle_collection_[track_id])
               << " Phi Before: " << pos_phi_before_nozzle_collection_[track_id]
               << " Phi After: " << pos_phi_after_nozzle_collection_[track_id]
               << " Delta Phi: "
               << (pos_phi_after_nozzle_collection_[track_id] - pos_phi_before_nozzle_collection_[track_id])
               << G4endl;
    }
}

void GenParticleCollector::setEnergyBeforeNozzle(const unsigned int &track_id, const G4double &energy) {
    energy_before_nozzle_collection_[track_id] = energy;
}

void GenParticleCollector::setEnergyAfterNozzle(const unsigned int &track_id, const G4double &energy) {
    energy_after_nozzle_collection_[track_id] = energy;
}

void GenParticleCollector::addEnergyDeposit(const unsigned int &track_id, const G4double &energy) {
    auto energy_deposit_it = energy_deposit_collection_.find(track_id);

    // Fetch previous energy deposit
    G4double deposit = 0.;

    if (energy_deposit_it != energy_deposit_collection_.end()) {
        deposit += energy_deposit_it->second;
    }

    // Accumulate energy
    deposit += energy;

    // Record new energy deposit
    energy_deposit_collection_[track_id] = deposit;
}

void GenParticleCollector::setEtaBeforeNozzle(const unsigned int &track_id, const G4double &pos_eta,
                                              const G4double &mom_eta) {
    pos_eta_before_nozzle_collection_[track_id] = pos_eta;
    mom_eta_before_nozzle_collection_[track_id] = mom_eta;
}

void GenParticleCollector::setEtaAfterNozzle(const unsigned int &track_id, const G4double &pos_eta,
                                             const G4double &mom_eta) {
    pos_eta_after_nozzle_collection_[track_id] = pos_eta;
    mom_eta_after_nozzle_collection_[track_id] = mom_eta;
}

void GenParticleCollector::setPhiBeforeNozzle(const unsigned int &track_id, const G4double &pos_phi,
                                              const G4double &mom_phi) {
    pos_phi_before_nozzle_collection_[track_id] = pos_phi;
    mom_phi_before_nozzle_collection_[track_id] = mom_phi;
}

void GenParticleCollector::setPhiAfterNozzle(const unsigned int &track_id, const G4double &pos_phi,
                                             const G4double &mom_phi) {
    pos_phi_after_nozzle_collection_[track_id] = pos_phi;
    mom_phi_after_nozzle_collection_[track_id] = mom_phi;
}
