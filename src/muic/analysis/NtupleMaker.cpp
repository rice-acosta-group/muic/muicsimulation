#include <G4SystemOfUnits.hh>
#include "G4ios.hh"
#include <g4root_defs.hh>
#include <G4GenericAnalysisManager.hh>

#include "muic/analysis/NtupleMaker.h"
#include "muic/utils/angles.h"
#include "muic/sensitive/MFSHit.h"

namespace muic {
    NtupleMaker::NtupleMaker() {
        auto *manager = G4GenericAnalysisManager::Instance();

        //
        manager->SetFirstNtupleId(0);
        manager->SetFirstNtupleColumnId(0);

        // Init metadata tree
        manager->CreateNtuple("metadata", "Metadata");
        manager->CreateNtupleIColumn("vsize_evt");
        manager->FinishNtuple();

        // Init event tree
        manager->CreateNtuple("events", "Events");

        // Counters
        manager->CreateNtupleIColumn("vsize_gen");
        manager->CreateNtupleIColumn("vsize_hit");

        // Gen Info
        manager->CreateNtupleDColumn("gen_e_before_nozzle", gen_e_before_nozzle_);
        manager->CreateNtupleDColumn("gen_e_after_nozzle", gen_e_after_nozzle_);
        manager->CreateNtupleDColumn("gen_e_deposited_nozzle", gen_e_deposited_nozzle_);

        manager->CreateNtupleDColumn("gen_pos_eta_before_nozzle", gen_pos_eta_before_nozzle_);
        manager->CreateNtupleDColumn("gen_pos_eta_after_nozzle", gen_pos_eta_after_nozzle_);
        manager->CreateNtupleDColumn("gen_pos_eta_scatter_nozzle", gen_pos_eta_scatter_nozzle_);
        manager->CreateNtupleDColumn("gen_mom_eta_before_nozzle", gen_mom_eta_before_nozzle_);
        manager->CreateNtupleDColumn("gen_mom_eta_after_nozzle", gen_mom_eta_after_nozzle_);
        manager->CreateNtupleDColumn("gen_mom_eta_scatter_nozzle", gen_mom_eta_scatter_nozzle_);

        manager->CreateNtupleDColumn("gen_pos_phi_before_nozzle", gen_pos_phi_before_nozzle_);
        manager->CreateNtupleDColumn("gen_pos_phi_after_nozzle", gen_pos_phi_after_nozzle_);
        manager->CreateNtupleDColumn("gen_pos_phi_scatter_nozzle", gen_pos_phi_scatter_nozzle_);
        manager->CreateNtupleDColumn("gen_mom_phi_before_nozzle", gen_mom_phi_before_nozzle_);
        manager->CreateNtupleDColumn("gen_mom_phi_after_nozzle", gen_mom_phi_after_nozzle_);
        manager->CreateNtupleDColumn("gen_mom_phi_scatter_nozzle", gen_mom_phi_scatter_nozzle_);

        // Hit Info
        manager->CreateNtupleIColumn("hit_station", hit_station);
        manager->CreateNtupleIColumn("hit_chamber", hit_chamber);
        manager->CreateNtupleDColumn("hit_energy", hit_energy);
        manager->CreateNtupleDColumn("hit_glob_x", hit_glob_x);
        manager->CreateNtupleDColumn("hit_glob_y", hit_glob_y);
        manager->CreateNtupleDColumn("hit_glob_z", hit_glob_z);
        manager->CreateNtupleDColumn("hit_glob_t", hit_glob_t);
        manager->CreateNtupleDColumn("hit_glob_eta", hit_glob_eta);
        manager->CreateNtupleDColumn("hit_glob_phi", hit_glob_phi);

        // Close event tree
        manager->FinishNtuple();

        // Open File
        auto file_open = manager->OpenFile("muic_ntuple.root");

        if (!file_open) {
            G4cout << "Failed to open file." << G4endl;
            return;
        }
    }

    NtupleMaker::~NtupleMaker() {
        // Do Nothing
    }

    void NtupleMaker::onRunStart() {
        // Do Nothing
    }

    void NtupleMaker::onRunEnd() {
        auto *analysis_manager = G4GenericAnalysisManager::Instance();

        G4cout << "Writing Ntuple." << G4endl;
        analysis_manager->Write();

        G4cout << "Closing Root File." << G4endl;
        analysis_manager->CloseFile();
    }

    void NtupleMaker::onEventStart() {
        // Increase number of events
        n_events_ += 1;

        // Nozzle Energy Deposit
        n_gen_ = 0;
        gen_e_before_nozzle_.clear();
        gen_e_after_nozzle_.clear();
        gen_e_deposited_nozzle_.clear();

        gen_pos_eta_before_nozzle_.clear();
        gen_pos_eta_after_nozzle_.clear();
        gen_pos_eta_scatter_nozzle_.clear();

        gen_mom_eta_before_nozzle_.clear();
        gen_mom_eta_after_nozzle_.clear();
        gen_mom_eta_scatter_nozzle_.clear();

        gen_pos_phi_before_nozzle_.clear();
        gen_pos_phi_after_nozzle_.clear();
        gen_pos_phi_scatter_nozzle_.clear();

        gen_mom_phi_before_nozzle_.clear();
        gen_mom_phi_after_nozzle_.clear();
        gen_mom_phi_scatter_nozzle_.clear();

        // Hits
        n_hit_ = 0;
        hit_station.clear();
        hit_chamber.clear();
        hit_energy.clear();
        hit_glob_x.clear();
        hit_glob_y.clear();
        hit_glob_z.clear();
        hit_glob_t.clear();
        hit_glob_eta.clear();
        hit_glob_phi.clear();
    }

    void NtupleMaker::onEventEnd() {
        auto *manager = G4GenericAnalysisManager::Instance();

        // Fill Metadata Ntuple
        manager->FillNtupleIColumn(0, 0, n_events_);
        manager->AddNtupleRow(0);

        // Fill Event ntuple
        manager->FillNtupleIColumn(1, 0, n_gen_);
        manager->FillNtupleIColumn(1, 1, n_hit_);
        manager->AddNtupleRow(1);
    }

    void NtupleMaker::addGenMuon(
            const G4double &energy_before_nozzle,
            const G4double &energy_after_nozzle,

            const G4double &pos_eta_before_nozzle,
            const G4double &pos_eta_after_nozzle,
            const G4double &mom_eta_before_nozzle,
            const G4double &mom_eta_after_nozzle,

            const G4double &pos_phi_before_nozzle,
            const G4double &pos_phi_after_nozzle,
            const G4double &mom_phi_before_nozzle,
            const G4double &mom_phi_after_nozzle
    ) {
        n_gen_ += 1;
        gen_e_before_nozzle_.push_back(energy_before_nozzle / GeV);
        gen_e_after_nozzle_.push_back(energy_after_nozzle / GeV);
        gen_e_deposited_nozzle_.push_back((energy_before_nozzle - energy_after_nozzle) / GeV);

        gen_pos_eta_before_nozzle_.push_back(pos_eta_before_nozzle);
        gen_pos_eta_after_nozzle_.push_back(pos_eta_after_nozzle);
        gen_pos_eta_scatter_nozzle_.push_back(pos_eta_after_nozzle - pos_eta_before_nozzle);

        gen_mom_eta_before_nozzle_.push_back(mom_eta_before_nozzle);
        gen_mom_eta_after_nozzle_.push_back(mom_eta_after_nozzle);
        gen_mom_eta_scatter_nozzle_.push_back(mom_eta_after_nozzle - mom_eta_before_nozzle);

        gen_pos_phi_before_nozzle_.push_back(pos_phi_before_nozzle / deg);
        gen_pos_phi_after_nozzle_.push_back(pos_phi_after_nozzle / deg);
        gen_pos_phi_scatter_nozzle_.push_back(wrap_phi(pos_phi_after_nozzle - pos_phi_before_nozzle) / deg);

        gen_mom_phi_before_nozzle_.push_back(mom_phi_before_nozzle / deg);
        gen_mom_phi_after_nozzle_.push_back(mom_phi_after_nozzle / deg);
        gen_mom_phi_scatter_nozzle_.push_back(wrap_phi(mom_phi_after_nozzle - mom_phi_before_nozzle) / deg);
    }

    void NtupleMaker::addHit(
            const MFSHit &hit
    ) {
        n_hit_ += 1;
        hit_station.push_back(hit.GetStation());
        hit_chamber.push_back(hit.GetChamber());
        hit_energy.push_back(hit.GetEnergy() * MeV / GeV);
        hit_glob_x.push_back(hit.GetPosition().getX() * mm / cm);  //  cm
        hit_glob_y.push_back(hit.GetPosition().getY() * mm / cm);  //  cm
        hit_glob_z.push_back(hit.GetPosition().getZ() * mm / cm);  //  cm
        hit_glob_t.push_back(hit.GetTime());  // ns
        hit_glob_eta.push_back(hit.GetPosition().getEta());
        hit_glob_phi.push_back(hit.GetPosition().getPhi() * rad / deg);  //  deg
    }

}
